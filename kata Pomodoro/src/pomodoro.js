const DEFAULT_DURATION = 25;
const ERROR_MESSAGE = "ERROR the timer can't be NULL value";
let timeWorkPomodoroStatus = false;
let userStartTimer;
let duration;




class Pomodoro{
  setupPomodoro(duration = DEFAULT_DURATION) {

      if (duration < 0) {
        duration = DEFAULT_DURATION;
     
      }else if (duration === null) {
        duration = ERROR_MESSAGE;
      }

    return duration;
    }

  startPomodoro(userStartTimer = false){
    
    let duration = this.setupPomodoro();
    
      if (userStartTimer === true ){
        this.controlTimers(duration , userStartTimer);
      }

  };

 async  controlTimers(duration , status ){
    
    let timerMilis = ((duration * 60) * 1000);

        if ( status === true ){
              //Starts Pomodoro here 
              timeWorkPomodoroStatus = true;

            setTimeout(()=>{
              //past timer , stops pomodoro 
               timeWorkPomodoroStatus = false;
            },timerMilis);
            
        };

      
  };

  controlStatus(){
   
    return timeWorkPomodoroStatus;
  }

}


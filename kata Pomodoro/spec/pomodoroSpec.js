describe("Making Kata Pomodoro", ()=> {

  const pomodoro = new Pomodoro();



  it('The Pomodoro have 25 minutes of duration by default ' , ()=>{
     
      duration = pomodoro.setupPomodoro();

      expect(duration).toEqual(DEFAULT_DURATION);
  });

  it('The Pomodoro`s time can have any duration ', ()=>{
  
      const minutesSetupByUserSimulated = 80; 

      duration = pomodoro.setupPomodoro(minutesSetupByUserSimulated);

       expect(duration).toEqual(minutesSetupByUserSimulated);
    });

  it("If the Pomodoro's duration is negative ,time fixed by default on 25 minutes" , ()=>{
    const DEFAULT_DURATION = 25; 
    let negativeSetupTime = -80 ;

      duration = pomodoro.setupPomodoro(negativeSetupTime);

     expect(duration).toEqual(DEFAULT_DURATION);
  });

  it("The Pomodoro's duration can be last 0 minutes " , ()=>{
    const ZERO_MINUTES = 0 ; 
    let userTimeupSelectZero = 0; 

     duration = pomodoro.setupPomodoro(userTimeupSelectZero);

      expect(duration).toEqual(ZERO_MINUTES);
  });

  it("The Pomodoro's timer can't be null " , ()=>{
    const ERROR_MESSAGE = "ERROR the timer can't be NULL value";
    const userSetupTimeOnNull = null;

      duration = pomodoro.setupPomodoro(userSetupTimeOnNull);

      expect(duration).toEqual(ERROR_MESSAGE);
  });

  it("The Pomodoro Starts Stopped ", () => {
    const statusTimeWork = false ;
 
    timeWorkPomodoroStatus = pomodoro.controlStatus(); 

    expect(timeWorkPomodoroStatus).toEqual(statusTimeWork);

  });

  it("Only the user can Start the pomodoro timer" , ()=>{
    const timerWorkStarted = false ;
    userStartTimer = false ; 
    
    pomodoro.startPomodoro(userStartTimer);
    timeWorkPomodoroStatus = pomodoro.controlStatus();
    
    expect(timeWorkPomodoroStatus).toEqual(timerWorkStarted);

  });
 
  it("When user starts the pomodoro , start's Time Work countDown " ,  ()=>{
    const timerWorkStarted = true;
    let countdownStatus;
    userStartTimer = true;

     pomodoro.startPomodoro(userStartTimer);
    countdownStatus = pomodoro.controlStatus();

    expect(countdownStatus).toEqual(timerWorkStarted);

  });

  it("The pomodoro ends when Worker timer ends " , ()=>{
    const timeOutStopped = false;
    const timerSelectedByUser = 1; 
    const timer = ((timerSelectedByUser * 60) * 1000);

    pomodoro.setupPomodoro(timerSelectedByUser);
    pomodoro.startPomodoro(true);


      
    await expect(timeWorkPomodoroStatus).toEqual(timeOutStopped);
    
  });
});
   